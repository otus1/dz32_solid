﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dz32_solidEx.calculate
{
    interface ICalculate
    {
        string GetTitle();
        double Calc(int a, int b, int c);
    }
}
