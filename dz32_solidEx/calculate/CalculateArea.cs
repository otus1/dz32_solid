﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dz32_solidEx.calculate
{
    class CalculateArea : ICalculate
    {
        public double Calc(int a, int b, int c)
        {
            var PoluPerimeter = (a + b + c)/2;  
            return Math.Sqrt(PoluPerimeter * (PoluPerimeter - a) * (PoluPerimeter - b) * (PoluPerimeter - c));
        }

        public string GetTitle()
        {
            return "Площадь";
        }
    }
}
