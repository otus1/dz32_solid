﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dz32_solidEx.calculate
{
    class CalculatePerimeter : ICalculate
    {
        public double Calc(int a, int b, int c)
        {
            return a + b + c;
        }

        public string GetTitle()
        {
            return "Периметр";
        }
    }
}
