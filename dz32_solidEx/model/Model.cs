﻿using dz32_solid.IO;
using dz32_solidEx.calculate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dz32_solidEx.model
{
    class Model
    {
        IRead read;
        IWrite write;
        ICalculate calc;

        bool isTryParams = true;
        int a = 0, b = 0, c = 0;
        double calcValue = 0;

        public Model(IWrite write, IRead read, ICalculate calc)
        {
            this.read = read;
            this.write = write;
            this.calc = calc;
        }
        public void Excecute()
        {
            Init();
            Calc();
            Done();
        }

        void Init()
        {
            while (isTryParams)
            {
                write.Print("Сторона a = ");
                a = read.GetInt();

                write.Print("Сторона b = ");
                b = read.GetInt();

                write.Print("Сторона c = ");
                c = read.GetInt();

                if ((a + b > c) && (a + c > b) && (b + c > a))
                    isTryParams = false;
                else
                {
                    write.PrintLN("Ошибка ввода. Стороны должны быть целыми числами");
                    write.PrintLN("");
                }
            }
        }
        void Calc()
        {
            calcValue = calc.Calc(a, b, c);
        }
        void Done()
        {
            write.PrintLN($"{calc.GetTitle()} = {calcValue}");
        }
    }
}
