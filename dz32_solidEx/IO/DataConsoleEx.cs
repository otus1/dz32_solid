﻿using dz32_solid.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dz32_solidEx.IO
{
    class DataConsoleEx: DataConsole
    {
        public override void PrintLN(string text)
        {
            Console.Write(text);
            Console.WriteLine();
        }
    }
}
