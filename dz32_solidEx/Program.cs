﻿using dz32_solid.IO;
using dz32_solidEx.calculate;
using dz32_solidEx.IO;
using dz32_solidEx.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dz32_solidEx
{
    class Program
    {
        static void Main(string[] args)
        {

            //IWrite write = new DataConsole();
            IWrite write = new DataConsoleEx();
            IRead read = new DataConsole();

            //ICalculate calc = new CalculatePerimeter();
            ICalculate calc = new CalculateArea();

            var model = new Model(write,read,calc);
            model.Excecute();            

            Console.ReadKey();
        }
    }
}
