﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dz32_solid
{
    class Program
    {
        static void Main(string[] args)
        {
            bool isTryParams = true;
            int a=0, b=0, c = 0;
            while (isTryParams)
            {
                Console.Write("Сторона a = ");
                int.TryParse(Console.ReadLine(), out a);

                Console.Write("Сторона b = ");
                int.TryParse(Console.ReadLine(),out b);

                Console.Write("Сторона c = ");
                int.TryParse(Console.ReadLine(), out c);

                if ((a + b > c) && (a + c > b) && (b + c > a))
                    isTryParams = false;
                else
                {
                    Console.WriteLine("Ошибка ввода. Стороны должны быть целыми числами");
                    Console.WriteLine("");
                }

            }

            var Perimeter = a + b + c;
            Console.WriteLine($"Периметр = {Perimeter}") ;

            var Area = Math.Sqrt(Perimeter*(Perimeter-a)*(Perimeter-b)*(Perimeter-c));
            Console.WriteLine($"Площадь = {Area}");

            Console.ReadKey();
        }
    }
}
