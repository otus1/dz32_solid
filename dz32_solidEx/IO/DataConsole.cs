﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dz32_solid.IO
{
    public class DataConsole : IWrite, IRead
    {
        public void Print(string text)
        {
            Console.Write(text);
        }

        public virtual void PrintLN(string text)
        {
            Console.WriteLine(text);
        }
        public int GetInt()
        {
            int result;
            int.TryParse(Console.ReadLine(), out result);
            return result;
        }
    }
}
